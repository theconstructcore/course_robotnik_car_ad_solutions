
##### Author - Nilesh Chopda

##### Project - Traffic Light Detection and Color Recognition using Tensorflow Object Detection API


### Import Important Libraries

import numpy as np
import os
import six.moves.urllib as urllib
import tarfile
import tensorflow as tf
from matplotlib import pyplot as plt
from PIL import Image as ImagePil
from os import path
from utils import label_map_util
from utils import visualization_utils as vis_util
import time
import cv2


class TrafficLightRecogniser(object):

    def __init__(self, MODEL_NAME, plot_flag=False):
        """
        :param PATH_TO_TEST_IMAGES_DIR: testing image directory
        :param MODEL_NAME: name of the model used in the task
        :return: commands: True: go, False: stop
        """
        self.plot_flag = plot_flag

        self.commands = []
        self.detected_traficlight_img = None
        self.color = "green"

        # What model to download
        MODEL_FILE = MODEL_NAME + '.tar.gz'
        DOWNLOAD_BASE = 'http://download.tensorflow.org/models/object_detection/'

        # Path to frozen detection graph. This is the actual model that is used for the object detection.
        PATH_TO_CKPT = MODEL_NAME + '/frozen_inference_graph.pb'

        # List of the strings that is used to add correct label for each box.
        PATH_TO_LABELS = 'mscoco_label_map.pbtxt'

        # number of classes for COCO dataset
        NUM_CLASSES = 90

        # --------Download model----------
        if path.isdir(MODEL_NAME) is False:
            opener = urllib.request.URLopener()
            opener.retrieve(DOWNLOAD_BASE + MODEL_FILE, MODEL_FILE)
            tar_file = tarfile.open(MODEL_FILE)
            for file in tar_file.getmembers():
                file_name = os.path.basename(file.name)
                if 'frozen_inference_graph.pb' in file_name:
                    tar_file.extract(file, os.getcwd())

        # --------Load a (frozen) Tensorflow model into memory
        self.detection_graph = tf.Graph()
        with self.detection_graph.as_default():
            od_graph_def = tf.compat.v1.GraphDef()
            
            with tf.compat.v2.io.gfile.GFile(PATH_TO_CKPT, 'rb') as fid:
                serialized_graph = fid.read()
                od_graph_def.ParseFromString(serialized_graph)
                tf.import_graph_def(od_graph_def, name='')

        # ---------Loading label map
        label_map = label_map_util.load_labelmap(PATH_TO_LABELS)
        categories = label_map_util.convert_label_map_to_categories(label_map,
                                                                    max_num_classes=NUM_CLASSES,
                                                                    use_display_name=True)
        self.category_index = label_map_util.create_category_index(categories)


        with self.detection_graph.as_default():
            with tf.compat.v1.Session(graph=self.detection_graph) as sess:
                # Definite input and output Tensors for self.detection_graph
                self.image_tensor = self.detection_graph.get_tensor_by_name('image_tensor:0')
                # Each box represents a part of the image where a particular object was detected.
                self.detection_boxes = self.detection_graph.get_tensor_by_name('detection_boxes:0')
                # Each score represent how level of confidence for each of the objects.
                # Score is shown on the result image, together with the class label.
                self.detection_scores = self.detection_graph.get_tensor_by_name('detection_scores:0')
                self.detection_classes = self.detection_graph.get_tensor_by_name('detection_classes:0')
                self.num_detections = self.detection_graph.get_tensor_by_name('num_detections:0')

    ### Function to Detect Traffic Lights and to Recognize Color

    def detect_traffic_lights(self, image_obj):
        """
        Detect traffic lights and draw bounding boxes around the traffic lights
        :param: image_obj: Is The image to be processed. It has to be CV2 compatible ( Numpy Array )
        """
        
        with self.detection_graph.as_default():
            with tf.compat.v1.Session(graph=self.detection_graph) as sess:

                # Expand dimensions since the model expects images to have shape: [1, None, None, 3]
                image_np_expanded = np.expand_dims(image_obj, axis=0)
                # Actual detection.
                (boxes, scores, classes, num) = sess.run(
                    [self.detection_boxes, self.detection_scores, self.detection_classes, self.num_detections],
                    feed_dict={self.image_tensor: image_np_expanded})

                stop_flag = self.read_traffic_lights_object(image_obj, np.squeeze(boxes), np.squeeze(scores),
                                                    np.squeeze(classes).astype(np.int32))
                if stop_flag:
                    # print('{}: stop'.format(image_path))  # red or yellow
                    self.commands.append(True)
                    cv2.putText(image_obj, 'Stop', (15, 25), cv2.FONT_HERSHEY_SIMPLEX, 1, (255, 0, 0), 1)
                else:
                    # print('{}: go'.format(image_path))
                    self.commands.append(False)
                    if self.color == "yellow":
                        cv2.putText(image_obj, 'WARNING', (15, 25), cv2.FONT_HERSHEY_SIMPLEX, 1, (255, 255, 0), 1)
                    else:
                        cv2.putText(image_obj, 'Go', (15, 25), cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 255, 0), 1)

                # Visualization of the results of a detection.
                self.plot_origin_image(image_obj, boxes, classes, scores, self.category_index)
                    

        return self.commands


    ### Function To Detect Red and Yellow Color
    # Here,we are detecting only Red and Yellow colors for the traffic lights as we need to stop the car when it detects these colors.

    def detect_red(self, img, Threshold=0.02):
        """
        detect red and yellow
        :param img:
        :param Threshold:
        :return:
        """

        desired_dim = (30, 90)  # width, height
        img = cv2.resize(np.array(img), desired_dim, interpolation=cv2.INTER_LINEAR)
        img_hsv = cv2.cvtColor(img, cv2.COLOR_RGB2HSV)


        # lower mask (0-10)
        lower_red = np.array([0, 70, 50])
        upper_red = np.array([10, 255, 255])
        mask0 = cv2.inRange(img_hsv, lower_red, upper_red)

        # upper mask (170-180)
        lower_red1 = np.array([170, 70, 50])
        upper_red1 = np.array([180, 255, 255])
        mask1 = cv2.inRange(img_hsv, lower_red1, upper_red1)

        # red pixels' mask
        mask = mask0 + mask1


        # cv2.imshow('RED',mask )
        # cv2.waitKey(1)


        # Compare the percentage of red values
        rate = np.count_nonzero(mask) / (desired_dim[0] * desired_dim[1])

        print(str(rate))
        if rate > Threshold:
            print("RED DETECTED")
            return True
        else:
            print("NO RED")
            return False
    
    def detect_yellow(self, img, Threshold=0.01):
        """
        detect red and yellow
        :param img:
        :param Threshold:
        :return:
        """

        desired_dim = (30, 90)  # width, height
        img = cv2.resize(np.array(img), desired_dim, interpolation=cv2.INTER_LINEAR)
        img_hsv = cv2.cvtColor(img, cv2.COLOR_RGB2HSV)
        
        # defining the Range of yellow color
        lower_yellow = np.array([5, 190, 190])
        upper_yellow = np.array([22, 255, 255])
        mask = cv2.inRange(img_hsv, lower_yellow, upper_yellow)

        # Compare the percentage of red values
        rate = np.count_nonzero(mask) / (desired_dim[0] * desired_dim[1])

        if rate > Threshold:
            return True
        else:
            return False



    ### Loading Image Into Numpy Array

    def load_image_into_numpy_array(self, image):
        (im_width, im_height) = image.size
        return np.array(image.getdata()).reshape(
            (im_height, im_width, 3)).astype(np.uint8)


    ### Read Traffic Light objects
    # Here,we will write a function to detect TL objects and crop this part of the image to recognize color inside the object. We will create a stop flag,which we will use to take the actions based on recognized color of the traffic light.

    def read_traffic_lights_object(self, image, boxes, scores, classes, max_boxes_to_draw=20, min_score_thresh=0.5,
                                traffic_ligth_label=10):
        # im_width, im_height = image.size
        im_height, im_width, channels = image.shape

        print("im_width="+str(im_width))
        print("im_height="+str(im_height))

        best_detected_img = None
        best_score = 0

        stop_flag = False
        for i in range(min(max_boxes_to_draw, boxes.shape[0])):
            if scores[i] > min_score_thresh and classes[i] == traffic_ligth_label:
                ymin, xmin, ymax, xmax = tuple(boxes[i].tolist())

                print("xmax="+str(xmax))
                print("xmin="+str(xmin))
                print("ymax="+str(ymax))
                print("ymin="+str(ymin))

                (left, right, top, bottom) = (xmin * im_width, xmax * im_width,
                                            ymin * im_height, ymax * im_height)


                print("left="+str(left))
                print("top="+str(top))
                print("right="+str(right))
                print("bottom="+str(bottom))

                print("int left="+str(int(left)))
                print("int top="+str(int(top)))
                print("int right="+str(int(right)))
                print("int bottom="+str(int(bottom)))

                #crop_img = image.crop((left, top, right, bottom))
                # [y:y+a, x:x+b]
                crop_img = image[int(top):int(bottom), int(left):int(right)]

                if scores[i] > best_score:
                    best_detected_img = crop_img
                    best_score = scores[i]
                    # We want to evaluate teh detection on the best rating traffic light detection
                    if self.detect_red(crop_img):
                        print("Red Detecetd setting flat to True")
                        stop_flag = True
                        self.color = "red"
                    else:
                        stop_flag = False
                    
                        if self.detect_yellow(crop_img):
                            print("YELLOW DETECTED!!!")
                            self.color = "yellow"                            
                        else:
                            print("...")
                            self.color = "green"
        
        if self.plot_flag:
            if best_detected_img is not None:
                img_rgb = cv2.cvtColor(best_detected_img, cv2.COLOR_BGR2RGB)
                cv2.imshow('RGB Image',img_rgb )
                cv2.waitKey(1)
            else:
                print("Empty IMAGE, no traffic light detected!!!!")
                

        return stop_flag


    ### Function to Plot detected image

    def plot_origin_image(self, image_np, boxes, classes, scores, category_index):
        # Size of the output images.
        IMAGE_SIZE = (12, 8)
        vis_util.visualize_boxes_and_labels_on_image_array(
            image_np,
            np.squeeze(boxes),
            np.squeeze(classes).astype(np.int32),
            np.squeeze(scores),
            category_index,
            min_score_thresh=.5,
            use_normalized_coordinates=True,
            line_thickness=3)
        if self.plot_flag:
            plt.figure(figsize=IMAGE_SIZE)
            plt.imshow(image_np)
            # save augmented images into hard drive
            # plt.savefig( 'output_images/ouput_' + str(idx) +'.png')
            plt.show()
        else:
            self.detected_traficlight_img = image_np

        
    def get_detected_traficlight_img(self):
        return self.detected_traficlight_img


### Let's detect Traffic lights in test_images directory

if __name__ == "__main__":
    # Specify number of images to detect
    Num_images = 17

    # Specify test directory path
    PATH_TO_TEST_IMAGES_DIR = './test_images'

    # Specify downloaded model name
    # MODEL_NAME ='ssd_mobilenet_v1_coco_11_06_2017'    # for faster detection but low accuracy
    MODEL_NAME = 'faster_rcnn_resnet101_coco_11_06_2017'  # for improved accuracy
    # --------test images------
    TEST_IMAGE_PATHS = [os.path.join(PATH_TO_TEST_IMAGES_DIR, 'img_{}.jpg'.format(i)) for i in range(1, Num_images + 1)]

    obj = TrafficLightRecogniser(MODEL_NAME, plot_flag=True)
    for image_path in TEST_IMAGE_PATHS:
        image = ImagePil.open(image_path)

        # the array based representation of the image will be used later in order to prepare the
        # result image with boxes and labels on it.
        image_np = obj.load_image_into_numpy_array(image)
        out_commands = obj.detect_traffic_lights(image_np)
        print(out_commands)  # commands to print action type, for 'Go' this will return True and for 'Stop' this will return False





