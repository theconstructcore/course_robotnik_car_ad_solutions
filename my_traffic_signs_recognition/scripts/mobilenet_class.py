import time
import warnings
warnings.filterwarnings('ignore')
import numpy as np
import os
import tensorflow as tf
from matplotlib import pyplot as plt
from PIL import Image
import glob as glob

import sys
import rospkg
# get an instance of RosPack with the default search paths
rospack = rospkg.RosPack()

path_to_pkg = rospack.get_path('my_traffic_signs_recognition')

# Append your Tensorflow object detection and darkflow directories to your path
PATH_TO_TENSORFLOW_OBJECT_DETECTION_FOLDER=os.path.join(path_to_pkg,"models/research")
PATH_TO_DARKFLOW_FOLDER=os.path.join(path_to_pkg,"darkflow")
sys.path.append(PATH_TO_TENSORFLOW_OBJECT_DETECTION_FOLDER) # ~/tensorflow/models/research/object_detection
sys.path.append(PATH_TO_DARKFLOW_FOLDER) # ~/darkflow

from object_detection.utils import label_map_util
from object_detection.utils import visualization_utils as vis_util

import cv2

# MODEL_NAME = 'faster_rcnn_inception_resnet_v2_atrous'
# MODEL_NAME = 'faster_rcnn_resnet_101'
# MODEL_NAME = 'faster_rcnn_resnet50'
# MODEL_NAME = 'faster_rcnn_inception_v2'
# MODEL_NAME = 'rfcn_resnet101'
# MODEL_NAME = 'ssd_inception_v2'
MODEL_NAME = 'ssd_mobilenet_v1'

# Path to frozen detection graph. This is the actual model that is used for the traffic sign detection.
MODEL_PATH = os.path.join(path_to_pkg,"scripts", "models", MODEL_NAME)
PATH_TO_CKPT = os.path.join(MODEL_PATH,'inference_graph','frozen_inference_graph.pb')

# List of the strings that is used to add correct label for each box.
PATH_TO_LABELS = os.path.join(path_to_pkg, 'scripts', 'gtsdb_data', 'gtsdb_label_map.pbtxt')

NUM_CLASSES = 3

# For the sake of simplicity we will use only 2 images:
# image1.jpg
# image2.jpg
# If you want to test the code with your images, just add path to the images to the TEST_IMAGE_PATHS.
PATH_TO_TEST_IMAGES_DIR = 'test_images'
TEST_IMAGE_PATHS = glob.glob(os.path.join(path_to_pkg, 'scripts', PATH_TO_TEST_IMAGES_DIR, '*.jpg'))

# Size, in inches, of the output images.
IMAGE_SIZE = (20, 20)


class TrafficSignalsRecognition(object):

    def __init__(self, plot_flag):
        
        self.plot_flag = plot_flag
        self.commands = []
        self.detected_traficlight_img = None

        self.detection_graph = tf.Graph()
        with self.detection_graph.as_default():
            od_graph_def = tf.compat.v1.GraphDef()
            # od_graph_def = tf.GraphDef()    
            with tf.compat.v2.io.gfile.GFile(PATH_TO_CKPT, 'rb') as fid:
                serialized_graph = fid.read()
                od_graph_def.ParseFromString(serialized_graph)
                tf.import_graph_def(od_graph_def, name='')

        label_map = label_map_util.load_labelmap(PATH_TO_LABELS)
        categories = label_map_util.convert_label_map_to_categories(label_map, max_num_classes=NUM_CLASSES, use_display_name=True)
        self.category_index = label_map_util.create_category_index(categories)
        print(label_map)

    def load_image_into_numpy_array(self, image):
        (im_width, im_height) = image.size
        return np.array(image.getdata()).reshape((im_height, im_width, 3)).astype(np.uint8)


    def plot_origin_image(self, image_np, boxes, classes, scores, category_index):
        # Size of the output images.
        IMAGE_SIZE = (20, 20)
        vis_util.visualize_boxes_and_labels_on_image_array(
            image_np,
            np.squeeze(boxes),
            np.squeeze(classes).astype(np.int32),
            np.squeeze(scores),
            category_index,
            min_score_thresh=.5,
            use_normalized_coordinates=True,
            line_thickness=3)        

        if self.plot_flag:
            img_rgb = cv2.cvtColor(image_np, cv2.COLOR_BGR2RGB)
            cv2.imshow('Traffic Sign detections',img_rgb )
            cv2.waitKey(0)
            cv2.destroyAllWindows()
        else:
            self.detected_traficlight_img = image_np

        
    def test(self):
        with self.detection_graph.as_default():
            with tf.compat.v1.Session(graph=self.detection_graph) as sess:
                for idx, image_path in enumerate(TEST_IMAGE_PATHS):
                    image = Image.open(image_path)

                    # the array based representation of the image will be used later in order to prepare the
                    # result image with boxes and labels on it.
                    image_np = self.load_image_into_numpy_array(image)

                    # Expand dimensions since the model expects images to have shape: [1, None, None, 3]
                    image_np_expanded = np.expand_dims(image_np, axis=0)
                    image_tensor = self.detection_graph.get_tensor_by_name('image_tensor:0')
                    # Each box represents a part of the image where a particular object was detected.
                    boxes = self.detection_graph.get_tensor_by_name('detection_boxes:0')
                    # Each score represent how level of confidence for each of the objects.
                    # Score is shown on the result image, together with the class label.
                    scores = self.detection_graph.get_tensor_by_name('detection_scores:0')
                    classes = self.detection_graph.get_tensor_by_name('detection_classes:0')
                    num_detections = self.detection_graph.get_tensor_by_name('num_detections:0')
                    # Actual detection.
                    (boxes, scores, classes, num_detections) = sess.run(
                        [boxes, scores, classes, num_detections],
                        feed_dict={image_tensor: image_np_expanded})
                    # Visualization of the results of a detection.
                    self.plot_origin_image(image_np, boxes, classes, scores, self.category_index)
    

    def detect_traffic_signs(self, image_np):
        """
        Detect traffic signals and draw bounding boxes around the traffic lights
        :param: image_obj: Is The image to be processed. It has to be CV2 compatible ( Numpy Array )
        """
        
        with self.detection_graph.as_default():
            with tf.compat.v1.Session(graph=self.detection_graph) as sess:
                for idx, image_path in enumerate(TEST_IMAGE_PATHS):

                    # Expand dimensions since the model expects images to have shape: [1, None, None, 3]
                    image_np_expanded = np.expand_dims(image_np, axis=0)
                    image_tensor = self.detection_graph.get_tensor_by_name('image_tensor:0')
                    # Each box represents a part of the image where a particular object was detected.
                    boxes = self.detection_graph.get_tensor_by_name('detection_boxes:0')
                    # Each score represent how level of confidence for each of the objects.
                    # Score is shown on the result image, together with the class label.
                    scores = self.detection_graph.get_tensor_by_name('detection_scores:0')
                    classes = self.detection_graph.get_tensor_by_name('detection_classes:0')
                    num_detections = self.detection_graph.get_tensor_by_name('num_detections:0')
                    # Actual detection.
                    (boxes, scores, classes, num_detections) = sess.run(
                        [boxes, scores, classes, num_detections],
                        feed_dict={image_tensor: image_np_expanded})
                    # Visualization of the results of a detection.
                    self.plot_origin_image(image_np, boxes, classes, scores, self.category_index)

        """
        item {
        id: 1
        name: 'prohibitory'
        }

        item {
        id: 2
        name: 'mandatory'
        }

        item {
        id: 3
        name: 'danger'
        }

        """
        print(str(classes))

        return None


    def get_detected_img(self):
        return self.detected_traficlight_img

if __name__ == "__main__":
    obj = TrafficSignalsRecognition()
    obj.test()
