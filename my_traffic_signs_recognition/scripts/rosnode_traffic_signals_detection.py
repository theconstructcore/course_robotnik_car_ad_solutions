#!/usr/bin/env python
import os
import rospy
from sensor_msgs.msg import Image
import rospkg
from cv_bridge import CvBridge, CvBridgeError
import cv2
from PIL import Image as ImagePil
import numpy as np
from mobilenet_class import TrafficSignalsRecognition
from image_publisher import ImagePublisher

class ROSTrafficLightRecogniser(object):

    def __init__(self, rgb_camera_topic, plot_flag=True, imgs_not_process=4):
        rospy.loginfo("Start ROSTrafficLightRecogniser Init process...")
        self.imgs_not_process = imgs_not_process
        self.img_proc_count = 0
        self._plot_flag = plot_flag

        self.current_img = None

        # get an instance of RosPack with the default search paths
        self.rate = rospy.Rate(5)

        self.bridge_object = CvBridge()
        rospy.loginfo("Start camera suscriber...")
        self.cam_topic = rgb_camera_topic
        self._check_cam_ready()
        self.image_sub = rospy.Subscriber(self.cam_topic,Image,self.camera_callback)
        

        self.img_publisher_obj = ImagePublisher( encoding=self.cam_img_check.encoding, 
                                        frame_id = self.cam_img_check.header.frame_id,
                                        output_image_topic = '/traffic_signs_detections/image_raw')
        
        self.traffic_signals_rec_obj = TrafficSignalsRecognition(plot_flag=False)
        
        rospy.loginfo("Finished ROSTrafficSignalsRecogniser Init process...Ready")



    def _check_cam_ready(self):
        self.cam_image = None
        while self.cam_image is None and not rospy.is_shutdown():
            try:
                self.cam_image = rospy.wait_for_message(self.cam_topic, Image, timeout=1.0)
                rospy.logdebug("Current "+self.cam_topic+" READY=>" + str(self.cam_image))

            except:
                rospy.logerr("Current "+self.cam_topic+" not ready yet, retrying.")

        self.cam_img_check = self.cam_image
        self.cam_image = self.rosimg_to_opencv(self.cam_img_check)

    def rosimg_to_opencv(self,data):
        try:
            # We select bgr8 because its the OpenCV encoding by default
            opencv_img = self.bridge_object.imgmsg_to_cv2(data, desired_encoding="bgr8")
        except CvBridgeError as e:
            print(e)

        return opencv_img
    



    def camera_callback(self,data):
        self.cam_image = data

    def loop(self):

        while not rospy.is_shutdown():
            rospy.logdebug("In loop...")
            self.recognise(self.cam_image)
            
            if self.current_img is not None:
                img_rgb = cv2.cvtColor(self.current_img, cv2.COLOR_BGR2RGB)
                self.img_publisher_obj.publish(img_rgb)
                # cv2.imshow('Traffic Light detections',img_rgb )
                # cv2.waitKey(1)

            self.rate.sleep()

    def recognise(self,data):

        if self.img_proc_count >= self.imgs_not_process:
            self.img_proc_count = 0
            rospy.loginfo("Processing Image")
            # Get a reference to webcam #0 (the default one)
            try:
                # We select bgr8 because its the OpneCV encoding by default
                video_capture = self.bridge_object.imgmsg_to_cv2(data, desired_encoding="rgb8")
            except CvBridgeError as e:
                print(e)

            small_frame = cv2.resize(video_capture, (0, 0), fx=0.5, fy=0.5)

            out_detection_info = self.traffic_signals_rec_obj.detect_traffic_signs(small_frame)
            print(out_detection_info)

            self.current_img = self.traffic_signals_rec_obj.get_detected_img()
        else:
            self.img_proc_count += 1
            rospy.logwarn("Not Processing Image")



def main():    
    
    traffic_signal_object = ROSTrafficLightRecogniser("/ueye_rgb_camera/image_raw", False)
    traffic_signal_object.loop()
    cv2.destroyAllWindows()

if __name__ == '__main__':
    rospy.init_node('traffic_signs_object_node', anonymous=True, log_level=rospy.DEBUG)
    main()