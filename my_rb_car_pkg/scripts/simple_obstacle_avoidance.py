#! /usr/bin/env python

import rospy
from sensor_msgs.msg import LaserScan
from geometry_msgs.msg import Twist
import copy
from math import radians, cos, sin, asin, sqrt

class SimpleObstacleAvoidance(object):

    def __init__(self, cmd_vel_topic="/cmd_vel", laser_topic="/hokuyo_front_laser/scan"):
        
        self.MIN_DISTANCE = 4.0
        self.lin_speed = 0.5
        self.turn_radius = 1.0

        self.cmd_vel_msgs = Twist()

        self.out_laser_msgs = LaserScan()
        self.out_laser_msgs.header.seq = 1
        self.out_laser_msgs.header.stamp = rospy.Time.now()
        self.out_laser_msgs.header.frame_id = "hokuyo_front_laser_link"

        self.laser_topic = laser_topic
        self._check_laser_ready()
        laser_sub = rospy.Subscriber(self.laser_topic, LaserScan, self.laser_callback)

        self._cmd_vel_topic = cmd_vel_topic
        self.cmd_vel_pub = rospy.Publisher(self._cmd_vel_topic, Twist, queue_size=1)

           
    def _check_laser_ready(self):
        laser_msg = None
        rospy.logdebug("Waiting for "+str(self.laser_topic)+" to be READY...")
        while laser_msg is None and not rospy.is_shutdown():
            try:
                laser_msg = rospy.wait_for_message(self.laser_topic, LaserScan, timeout=1.0)
                rospy.logdebug("Current "+str(self.laser_topic)+"READY=>")

            except:
                rospy.logerr("Current "+str(self.laser_topic)+" not ready yet, retrying for getting odom")
        
        self.laser_ranges = laser_msg.ranges

    def laser_callback(self, msg):
        self.laser_ranges = msg.ranges

        self.right_range_min = min(msg.ranges[0:int(len(msg.ranges)/5)])
        self.center_range_min = min(msg.ranges[2*int(len(msg.ranges)/5):3*int(len(msg.ranges)/5)])
        self.left_range_min = min(msg.ranges[4*int(len(msg.ranges)/5):])

        # We make this more sensible on the sides
        o_left = self.left_range_min <= self.MIN_DISTANCE / 4.0
        o_right = self.right_range_min <= self.MIN_DISTANCE / 4.0
        o_center = self.center_range_min <= self.MIN_DISTANCE 

        action = "nothing"

        if o_center:
            if o_right:
                if o_left:
                    action = "stop"
                else:
                    action = "left"
            else:
                action = "right"
        else:
            action = "forwards"
            

        rospy.loginfo("Action=="+str(action)+"["+str(self.left_range_min)+","+str(self.center_range_min)+","+str(self.right_range_min)+"]")

                
        self.cmd_vel_msgs = Twist()
        # Set the cmd vel msg based on the action
        if action == "left":
            self.cmd_vel_msgs.angular.z = self.turn_radius
            self.cmd_vel_msgs.linear.x = self.lin_speed
        elif action == "right":
            self.cmd_vel_msgs.angular.z = -1.0 * self.turn_radius
            self.cmd_vel_msgs.linear.x = self.lin_speed
        elif action == "forwards":
            self.cmd_vel_msgs.angular.z = 0.0
            self.cmd_vel_msgs.linear.x = self.lin_speed * 1.0
        else:
            pass


    def loop(self):
        rate_obj = rospy.Rate(5.0)
        while not rospy.is_shutdown():
            rospy.loginfo("Publishing cmd_vel="+str(self.cmd_vel_msgs))
            self.cmd_vel_pub.publish(self.cmd_vel_msgs)
            rate_obj.sleep()
    



def main():
    rospy.init_node('obstacle_avoidance')
    obj =  SimpleObstacleAvoidance()
    obj.loop()
    # rospy.spin()

if __name__=="__main__":
    main()