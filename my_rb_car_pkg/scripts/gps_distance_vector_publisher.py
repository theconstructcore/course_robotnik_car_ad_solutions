#! /usr/bin/env python

import rospy
from sensor_msgs.msg import NavSatFix
from std_msgs.msg import Float64
import copy
from math import radians, cos, sin, asin, sqrt

class GPSVectorPublisher(object):

    def __init__(self, des_gps_loc="/gps/desired_location", gps_error=0.003):
        
        # The default value is the normal value for any GPS, 3 meters ( 0.003 Km )
        self.GPS_ERROR = gps_error
        self.is_in_des_loc = False

        self.gps_topic = "/gps/fix"
        self._check_gps_ready()
        gps_sub = rospy.Subscriber(self.gps_topic, NavSatFix, self.gps_callback)

        # We initialise it with a null value
        self.set_desired_gps_location(des_lat=0.0,des_lon=0.0)
        self.des_dist = self.distance(lat1=self._des_lat, lat2=self.lat, lon1=self._des_lon, lon2=self.lon)
        self.des_gps_loc = des_gps_loc
        gps_des_sub = rospy.Subscriber(self.des_gps_loc, NavSatFix, self.gps_des_callback)
    
           
    def _check_gps_ready(self):
        self.gps = None
        rospy.logdebug("Waiting for "+str(self.gps_topic)+" to be READY...")
        while self.gps is None and not rospy.is_shutdown():
            try:
                self.gps = rospy.wait_for_message(self.gps_topic, NavSatFix, timeout=1.0)
                rospy.logdebug("Current "+str(self.gps_topic)+"READY=>")

            except:
                rospy.logerr("Current "+str(self.gps_topic)+" not ready yet, retrying for getting odom")
        self.lat = self.gps.latitude
        self.lon = self.gps.longitude

    def gps_callback(self, msg):
        self.lat = msg.latitude
        self.lon = msg.longitude
        rospy.logdebug("LAT"+str(self.lat))
        rospy.logdebug("LON"+str(self.lon))

        # We calculate distance to desired location
        self.des_dist = self.distance(lat1=self._des_lat, lat2=self.lat, lon1=self._des_lon, lon2=self.lon)

        # We check if its in the right location
        self.is_in_des_loc = self.des_dist <= self.GPS_ERROR

        if self.is_in_des_loc:
            rospy.loginfo("SUCCESS DES DIST=="+str(self.des_dist))
        else:
            rospy.logerr("NOT THERE YET DES DIST=="+str(self.des_dist))

    def gps_des_callback(self,msg):
        self.set_desired_gps_location(des_lat=msg.latitude,des_lon=msg.longitude)
        rospy.loginfo("UPDATED DESIRED LAT"+str(self.lat))
        rospy.loginfo("UPDATED DESIRED LON"+str(self.lon))

    def set_desired_gps_location(self,des_lat,des_lon):
        """
        We set the GPS postion where we want the car to be
        EX: latitude: 39.507840029275954
            longitude: -0.46182210293333387

        """
        self._des_lat = des_lat
        self._des_lon = des_lon

    def distance(self, lat1, lat2, lon1, lon2):
        """
        Using the great circle distance approach.
        """
        
        # The math module contains a function named
        # radians which converts from degrees to radians.
        lon1 = radians(lon1)
        lon2 = radians(lon2)
        lat1 = radians(lat1)
        lat2 = radians(lat2)
        
        # Haversine formula
        dlon = lon2 - lon1
        dlat = lat2 - lat1
        a = sin(dlat / 2)**2 + cos(lat1) * cos(lat2) * sin(dlon / 2)**2
    
        c = 2 * asin(sqrt(a))
        
        # Radius of earth in kilometers. Use 3956 for miles
        r = 6371
        
        # calculate the result
        return(c * r)



def main():
    rospy.init_node('gps_vector_pub')
    obj = GPSVectorPublisher()
    rospy.spin()

if __name__=="__main__":
    main()